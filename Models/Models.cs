﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PLFrontEnd.Models
{
    public class Models
    {
        public class TableModel
        {
            [Display(Name = "Player Name")]
            public string PlayerName { get; set; }
            [Display(Name = "Table")]
            public List<string> Table { get; set; }
            public int _id { get; set; }
        }

        public class TableRootObject
        {
            [JsonProperty("standings")]
            public List<StandingItems> Results { get; set; }

            [JsonProperty("season")]
            public SeasonItem Matchday { get; set; }
        }

        public class StandingItems
        {
            [JsonProperty("table")]
            public List<TeamItem> standing;
            public string type;
        }

        public class SeasonItem
        {
            public string id;
            public string startDate;
            public string endDate;
            public int currentMatchday;
        }

        public class TeamItem
        {
            [JsonProperty("position")]
            public int position;
            [JsonProperty("team")]
            public TeamInfo team;
            [JsonProperty("playedGames")]
            public int playedGames;
            [JsonProperty("points")]
            public int points;
        }

        public class TeamInfo
        {
            [JsonProperty("id")]
            public int id;
            [JsonProperty("name")]
            public string name;
        }

        public class GameweekTable
        {
            [Display(Name = "Gameweek")]
            public int Gameweek { set; get; }
            [Display(Name = "Table")]
            public List<TableObject> Table { get; set; }
            public int _id { get; set; }
        }

        public class TableObject
        {
            [Display(Name = "Team Name")]
            public string TeamName { get; set; }
            [Display(Name = "Pos")]
            public int Position { get; set; }
            [Display(Name = "Played")]
            public int PlayedMatches { get; set; }
            [Display(Name = "Points")]
            public string Points { get; set; }
        }

        public class FantasyRootObjects
        {
            [JsonProperty("standings")]
            public FantasyStandingObject standings;
        }

        public class FantasyStandingObject
        {
            [JsonProperty("results")]
            public List<FantasyResultObject> results;
        }

        public class FantasyResultObject
        {
            [DisplayName("Team")]
            [JsonProperty("entry_name")]
            public string Entry_name { get; set; }
            [DisplayName("Player Name")]
            [JsonProperty("player_name")]
            public string Player_name { get; set; }
            [DisplayName("Pos")]
            [JsonProperty("rank")]
            public int Rank { get; set; }
            [DisplayName("Last Week")]
            [JsonProperty("last_rank")]
            public int Last_rank { get; set; }
            [JsonProperty("movement")]
            public string movement;
            [DisplayName("Total")]
            [JsonProperty("total")]
            public int Total { get; set; }
            [DisplayName("Gameweek")]
            [JsonProperty("event_total")]
            public int Event_total { get; set; }
        }

        public class CompleteHomeObject
        {
            public GameweekTable GameweekTable { get; set; }
            public List<TableModel> TableModel { get; set; }
            public List<Tuple<string, int>> PointResult { get; set; }
            public FantasyRootObjects FantasyStanding { get; set; }
            public List<Tuple<string, List<FantasyPlayerObject>>> CementStanding { get; set; }
            public Dictionary<string, List<Tuple<string, int>>> ProfitCalculation { get; set; }
        }

        public class FantasyPlayerDatabaseRootObject
        {
            [JsonProperty("elements")]
            public List<FantasyPlayerObject> elements;
        }

        public class FantasyPlayerObject
        {
            [JsonProperty("id")]
            public int id;

            [JsonProperty("first_name")]
            public string first_name;

            [JsonProperty("second_name")]
            public string second_name;

            [JsonProperty("total_points")]
            public int total_points;

            [JsonProperty("web_name")]
            public string web_name;

            [JsonProperty("element_type")]
            public int element_type;
        }
    }
}