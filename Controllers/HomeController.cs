﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PLFrontEnd.Models;
using static PLFrontEnd.Models.Models;

namespace PLFrontEnd.Controllers
{
    public class HomeController : Controller
    {
        public async Task<IActionResult> Index()
        {
            CompleteHomeObject compObj = new CompleteHomeObject();

            using (HttpClient client = new HttpClient())
            {
                string url = "";
                HttpResponseMessage response = null;

                url = "http://pho3nixapi.azurewebsites.net/api/PremierLeague/GetCurrentPremierLeagueTable";
                response = await client.GetAsync(url);
                GameweekTable table = new GameweekTable();
                if (response.IsSuccessStatusCode)
                {
                    compObj.GameweekTable = await response.Content.ReadAsAsync<GameweekTable>();
                }

                url = "http://pho3nixapi.azurewebsites.net/api/PremierLeague/GetPlayerTables";
                response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    compObj.TableModel = await response.Content.ReadAsAsync<List<TableModel>>();
                }

                url = "http://pho3nixapi.azurewebsites.net/api/PremierLeague/CalculatePlayerPoints";
                response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var test = response.Content.ReadAsStringAsync().Result;
                    var obj = JsonConvert.DeserializeObject<List<Tuple<string, int>>>(test);
                    compObj.PointResult = obj;
                }

                url = "http://pho3nixapi.azurewebsites.net/api/PremierLeague/GetPremierLeagueFantasyStanding";
                response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var test = response.Content.ReadAsStringAsync().Result;
                    var obj = JsonConvert.DeserializeObject<FantasyRootObjects>(test);

                    foreach (var item in obj.standings.results)
                    {
                        if (item.Player_name.Contains("Monteiro"))
                        {
                            item.Player_name = "André";
                        }
                        if (item.Player_name.Contains("Stefan"))
                        {
                            item.Player_name = "Stefan";
                        }
                        if (item.Player_name.Contains("Edward"))
                        {
                            item.Player_name = "Edward";
                        }
                        if (item.Player_name.Contains("Fredric"))
                        {
                            item.Player_name = "Fredric";
                        }
                        if (item.Player_name.Contains("Anthonio"))
                        {
                            item.Player_name = "Anthonio";
                        }
                    }

                    compObj.FantasyStanding = obj;
                }

                url = "http://pho3nixapi.azurewebsites.net/api/PremierLeague/GetCementEleven";
                response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    var test = response.Content.ReadAsStringAsync().Result;
                    var obj = JsonConvert.DeserializeObject<List<Tuple<string, List<FantasyPlayerObject>>>>(test);
                    var sorted = obj.OrderBy((x => x.Item2.Sum(x2 => x2.total_points)));
                    compObj.CementStanding = obj;
                }

                compObj.ProfitCalculation = CalculatePlayerProfit(compObj);

                response.Dispose();
            }

            return View(compObj);
        }

        private Dictionary<string, List<Tuple<string, int>>> CalculatePlayerProfit(CompleteHomeObject comp)
        {
            List<Tuple<string, int>> stefanList = new List<Tuple<string, int>>()
            {
                 new Tuple<string, int>( "Insats: Tabellen", -100),
                  new Tuple<string, int>( "Insats: Fantasy", -100),
                   new Tuple<string, int>( "Insats: Cement", -100)
            };

            List<Tuple<string, int>> edwardList = new List<Tuple<string, int>>()
            {
                 new Tuple<string, int>( "Insats: Tabellen", -100),
                  new Tuple<string, int>( "Insats: Fantasy", -100),
                   new Tuple<string, int>( "Insats: Cement", -100)
            };

            List<Tuple<string, int>> andréList = new List<Tuple<string, int>>()
            {
                 new Tuple<string, int>( "Insats: Tabellen", -100),
                  new Tuple<string, int>( "Insats: Fantasy", -100),
                   new Tuple<string, int>( "Insats: Cement", -100)
            };

            List<Tuple<string, int>> anthonioList = new List<Tuple<string, int>>()
            {
                 new Tuple<string, int>( "Insats: Tabellen", -100),
                  new Tuple<string, int>( "Insats: Fantasy", -100)
            };

            List<Tuple<string, int>> fredricList = new List<Tuple<string, int>>()
            {
                 new Tuple<string, int>( "Insats: Fantasy", -100)
            };

            Dictionary<string, List<Tuple<string, int>>> returnObj = new Dictionary<string, List<Tuple<string, int>>>()
            {
                {"Stefan", stefanList },
                {"Edward", edwardList },
                {"André", andréList },
                {"Anthonio", anthonioList },
                {"Fredric", fredricList }
            };

            int highestTablePoint = comp.PointResult.Max(x => x.Item2);
            var tableLeaders = comp.PointResult.Where(x => x.Item2 == highestTablePoint);

            foreach (var item in tableLeaders)
            {
                double profilSplit = 400.0 / tableLeaders.Count();
                int profit = Convert.ToInt32(profilSplit);

                returnObj[item.Item1].Add(new Tuple<string, int>("Vinst: Tabellen", profit));
            }

            var highestFantasyPoints = comp.FantasyStanding.standings.results.Max(x => x.Total);
            var fantasyLeaders = comp.FantasyStanding.standings.results.Where(x => x.Total == highestFantasyPoints);

            foreach (var item in fantasyLeaders)
            {
                double profilSplit = 500.0 / fantasyLeaders.Count();
                int profit = Convert.ToInt32(profilSplit);
                
                returnObj[item.Player_name].Add(new Tuple<string, int>("Vinst: Fantasy", profit));
            }

            var highestCementPoints = comp.CementStanding.Max(x => x.Item2.Sum(x2 => x2.total_points));
            var cementLeaders = comp.CementStanding.Where(x => x.Item2.Sum(x2 => x2.total_points) == highestCementPoints);

            foreach (var item in cementLeaders)
            {
                double profilSplit = 300.0 / fantasyLeaders.Count();
                int profit = Convert.ToInt32(profilSplit);
                
                returnObj[item.Item1].Add(new Tuple<string, int>("Vinst: Cement", profit));
            }

            return returnObj;
        }
    }
}
